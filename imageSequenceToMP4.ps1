# rename all the images
$i = 1
Get-ChildItem *.jpg | %{Rename-Item $_ -NewName ('image_{0:D4}.jpg' -f $i++)}

# make the video
#-i = input
# -c:v = codec:video
# -crf = constant rate factor, ie: lossiness scale. 0 is lossless, 18 is visually lossless
# -preset = speed of processing. slower = smaller file size
# -c:a = codec:audio
# -b:a = bitrate:audio
ffmpeg -i w_%04d.jpg -c:v libx264 -crf 0 -preset veryslow -c:a libmp3lame -b:a 320k out.mp4